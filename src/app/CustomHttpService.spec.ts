import {
  HttpClientTestingModule,
  HttpTestingController
} from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';

import { CustomHttpService } from './CustomHttpService';

describe('CustomHttpService', () => {
  let service: CustomHttpService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [CustomHttpService]
    });

    // inject the service
    service = TestBed.get(CustomHttpService);
    httpMock = TestBed.get(HttpTestingController);
  });

  it('should get the correct star wars character', () => {
    service.post<any>({data:'data'}).subscribe((data: any) => {
      console.log("data better", data)
      expect('sky').toBe('sky');
    });

     const req = httpMock.expectOne(`https://api2.locard.co.id/f3/v1/member/guest_login`, 'call to api');
     expect(req.request.method).toBe('POST');

    req.flush({
      name: 'Luke Skywalker'
    });

    httpMock.verify();
  });

  
  // it('should put the correct data', () => {
  //   service.put <
  //     any >
  //     (3, { firstname: 'firstname' }).subscribe((data: any) => {
       
  //       expect(data.firstname).toBe('firstname');
  //     });
  
  //   const req = httpMock.expectOne(
  //     `http://replace.with.api/anything/3`,
  //     'put to api'
  //   );
  //   expect(req.request.method).toBe('PUT');
  
  //   req.flush({
  //     firstname: 'firstname'
  //   });
  
  //   httpMock.verify();
  // });
  
  // it('should delete the correct data', () => {
  //   service.delete(3).subscribe((data: any) => {
  //     expect(data).toBe(3);
  //   });
  
  //   const req = httpMock.expectOne(
  //     `http://replace.with.api/anything/3`,
  //     'delete to api'
  //   );
  //   expect(req.request.method).toBe('DELETE');
  
  //   req.flush(3);
  
  //   httpMock.verify();
  // });

});