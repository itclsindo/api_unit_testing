import { Injectable, Component, OnInit } from '@angular/core'
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http'
import { Observable } from 'rxjs'
import { api_url } from './environtment.api'

interface Result {
  error: any,
  result: any
}

@Injectable({
  providedIn: 'root',

})
@Component({
  selector: 'master-service',
  template: ``
})

export class MasterService {
  public api_url: string = ''
  public http: HttpClient

  constructor(public httpGet: HttpClient) {
    this.http = httpGet
    this.api_url = api_url()
  }

  public async get(_params, _url, additionalHeaders?: any) {

    let result = null
    try {
      result = await this.getRest(_params, _url, additionalHeaders).toPromise()
    } catch (error) {
      this.checkStatus(error)
      throw new Error(JSON.stringify(error, null, 2))
    }

    return result
  }

  public async post(_params, _url, headers?: any) {
    let result
    try {
      result = await this.postRest(_params, _url, headers).toPromise()
      console.log('result = ', result)
    } catch (error) {
      this.checkStatus(error)
      console.log('error = ', error)

      throw new Error(JSON.stringify(error, null, 2))
      // result = {error:error.error.error,error_message: error.error.error}
    }

    //console.log('result error', result)
    // let result = await this.postRest(_params, _url).toPromise()

    return result
  }

  public async add(_params, _url, headers?: any) {
    let result
    try {

      result = await this.postRest(_params, _url, status).toPromise()
      console.log('result = ', result)
    } catch (error) {
      this.checkStatus(error)
      throw new Error(JSON.stringify(error, null, 2))
      // result = {error:error.error.error,error_message: error.error.error}
    }

    //console.log('result error', result)
    // let result = await this.postRest(_params, _url).toPromise()

    return result
  }

  public async update(_params, _url, headers?: any) {
    //let result = await this.updateRest(_params, _url).toPromise()
    let result = null
    try {
      result = await this.updateRest(_params, _url).toPromise()
    } catch (error) {
      this.checkStatus(error)
      throw new Error(JSON.stringify(error, null, 2))
    }

    return result
  }

  public async delete(_params, _url, headers?: any) {
    //let result = await this.deleteRest(_params, _url).toPromise()
    let result = null
    try {
      result = await this.deleteRest(_params, _url).toPromise()
    } catch (error) {
      this.checkStatus(error)
      throw new Error(JSON.stringify(error, null, 2))
    }
    return result
  }

  private getRest(_params, _url, additionalHeaders?: any): Observable<Result[]> {

    // if(myStatus == 'true'){
    let httpParams = new HttpParams()
    let arrayParams = Object.assign({}, _params)
    httpParams = httpParams.append('request', JSON.stringify(arrayParams))

    let headerParams = {
      'Content-Type': 'application/json',
      // 'Authorization': myToken,
      //'Authorization': 'my-auth-token',
    }

    if (additionalHeaders) {
      headerParams = {...headerParams, ...additionalHeaders}
    }
    console.log('additionalHeaders headerParams', headerParams)

    let httpOpt = { 
      headers: new HttpHeaders(headerParams) ,
      params: {}
    }


    if (_params) {
      httpOpt.params = _params
    }
    const uri = _url
    //let res = this.http.get<Result[]>(uri, httpOpt)
    console.log("URI httpOpt", httpOpt)
    return this.http.get<Result[]>(uri, httpOpt)
    // } else {
    //   localStorage.removeItem('isLoggedin')
    // }
  }

  private postRest(_params, _url, headers?: any): Observable<Result[]> {

    let httpParams = new HttpParams()
    let arrayParams = Object.assign({}, _params)
    //console.log('data object 1 = ', arrayParams)
    httpParams = httpParams.append('request', JSON.stringify(arrayParams))
    console.log('data object 2 = ', httpParams)
    const httpOpt = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        // 'Authorization': myToken,
        //'Authorization': 'my-auth-token',

      }),
      // params  : httpParams,
    }

    const uri = _url
    //let res = this.http.post<Result[]>(uri,arrayParams, httpOpt)
    //console.log('data = ',this.http.put<Result[]>(uri, arrayParams, httpOpt))
    return this.http.post<Result[]>(uri, arrayParams, httpOpt)

  }
  private updateRest(_params, _url, headers?: any): Observable<Result[]> {

    let myToken = localStorage.getItem('tokenlogin')
    let myStatus = localStorage.getItem('isLoggedin')

    if (myStatus == 'true') {
      if (_params) {

        let httpParams = new HttpParams()
        let arrayParams = Object.assign({}, _params)
        httpParams = httpParams.append('request', JSON.stringify(arrayParams))

        const httpOpt = {
          headers: new HttpHeaders({
            'Content-Type': 'application/json',
            'Authorization': myToken,
            //'Authorization': 'my-auth-token',
          }),
          // params  : httpParams
        }

        const uri = _url
        //let res = this.http.put<Result[]>(uri, arrayParams, httpOpt)
        return this.http.put<Result[]>(uri, arrayParams, httpOpt)
      }
    } else {
      localStorage.removeItem('isLoggedin')
    }
  }

  private deleteRest(_params, _url, headers?: any): Observable<Result[]> {
    let myToken = localStorage.getItem('tokenlogin')
    let myStatus = localStorage.getItem('isLoggedin')

    if (myStatus == 'true') {
      if (_params) {

        let httpParams = new HttpParams()
        let arrayParams = Object.assign({}, _params)
        httpParams = httpParams.append('request', JSON.stringify(arrayParams))

        const httpOpt = {
          headers: new HttpHeaders({
            'Content-Type': 'application/json',
            'Authorization': myToken,
            //'Authorization': 'my-auth-token',
          }),
          params: httpParams
        }

        const uri = _url
        //let res = this.http.delete<Result[]>(uri,httpOpt)
        return this.http.delete<Result[]>(uri, httpOpt)
      }
    } else {
      localStorage.removeItem('isLoggedin')
    }
  }

  private searchResultRest(_prms, _url): Observable<Result[]> {
    let myToken = localStorage.getItem('tokenlogin')
    let myStatus = localStorage.getItem('isLoggedin')

    if (myStatus == 'true') {
      if (_prms) {

        let httpParams = new HttpParams()
        _prms.search = Object.assign({}, _prms.search)
        _prms.order_by = Object.assign({}, _prms.order_by)

        let arrayParams = Object.assign({}, _prms)
        httpParams = httpParams.append('request', JSON.stringify(arrayParams))

        const httpOpt = {
          headers: new HttpHeaders({
            'Content-Type': 'application/json',
            'Authorization': myToken,
          }),
          params: httpParams
        }

        const uri = _url
        //let res = this.http.get<Result[]>(uri, httpOpt)
        return this.http.get<Result[]>(uri, httpOpt)
      }
    } else {
      localStorage.removeItem('isLoggedin')
    }
  }

  public async searchResult(_params, _url) {
    let result
    try {
      result = await this.searchResultRest(_params, _url).toPromise()
    } catch (error) {
      this.checkStatus(error)
      throw new Error(JSON.stringify(error, null, 2))
    }
    return result
  }

  private checkStatus(error) {
    let loginStatus = localStorage.getItem('isLoggedin')
    if (error.status === 401 && loginStatus == 'true') {
      localStorage.removeItem('isLoggedin')
      localStorage.removeItem('tokenlogin')
      window.location.reload()
    }
  }

}