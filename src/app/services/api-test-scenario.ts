import { MasterService } from './master.service';
import { Injectable, Component } from '@angular/core';
import { VirtualTimeScheduler } from 'rxjs';
import { appDebug } from './environtment.api';

@Injectable({
    providedIn: 'root',
    
  })
@Component({
selector: 'ApiTestScenario-service',
template: ``})

export class ApiTestScenario {

    scenarioTitle   : String;
    debug           : boolean= false;
    resultArray     : Array<any> = [];
    bodyObject      : any;
    describe        : String;

    // masterService   : MasterService;

        
    constructor(private masterService: MasterService){}

    setScenario(
        _title      : String, 
        _bodyObject : any, 
        _debug?     : boolean,
        ){
        this.scenarioTitle  = _title;
        this.bodyObject     = _bodyObject;
        
        this.debug = appDebug;
        
        return this
    }

    getScenarioTitle(){
        return this.scenarioTitle
    }

    async setDescribe(_text: String, _func: Function){
        //doFunction();
        this.describe = _text;
        await _func(this);
        return _text;
    }

    getDescribe(){
        return this.describe;
    }
    async runAPICall(_apiObject: API_Object, _func?: Function){
        giveConsoleComment("call API Debug", _apiObject)
        giveConsoleComment("master Service", this.masterService)

        try{
            giveConsoleComment("log comment object", _apiObject.method)

            switch(_apiObject.method){
                case 'GET' : {
                    return await this.get(_apiObject, _func)
                    break;
                }
                case 'POST' : {
                    return await this.post(_apiObject, _func)
                    break;
                }
            }
        }
        catch(e){
            return new Promise((resolve) =>{
                setTimeout(()=>{
                    giveConsoleComment("ERROR RESPONSE", (<Error>e).message)
                    const data : RESP_Data = {
                        status       : 'NOK',
                        urlTarget    : _apiObject.urlTarget,
                        responseBody : JSON.stringify(JSON.parse((<Error>e).message), null,  2),
                        requestBody  : JSON.stringify(_apiObject.requestParams, null,  2)
                    }
                   
                    // this.resultArray.push(data);
                    
                    resolve(data)
                }, 1000)
            });
        }
        
    }


    async get(_apiObject, _func?: Function){

        giveConsoleComment("HELLO API OBJECT", _apiObject)

        let resp = await this.masterService.get(_apiObject.requestParams, _apiObject.urlTarget, _apiObject.headers);
        return new Promise((resolve) =>{
            setTimeout(()=>{
                giveConsoleComment("COMMENT", JSON.stringify(_apiObject, null,  2))
                let data : RESP_Data = {
                    status       : 'OK',
                    urlTarget    : _apiObject.urlTarget,
                    responseBody : JSON.stringify(resp, null,  2),
                    requestBody  : JSON.stringify(_apiObject.requestParams, null,  2)
                }

                if(_apiObject.headers){
                    data.headers = JSON.stringify(_apiObject.headers, null, 2)
                }
               
                if(_func)
                    _func(data, this)

                resolve(data)
            }, 1000)
        });
    }

    async post(_apiObject, _func?: Function){
        let resp = await this.masterService.post(_apiObject.requestParams, _apiObject.urlTarget, _apiObject.headers);
        return new Promise((resolve) =>{
            setTimeout(()=>{
                giveConsoleComment("COMMENT", JSON.stringify(_apiObject, null,  2))
                const data : RESP_Data = {
                    status       : 'OK',
                    urlTarget    : _apiObject.urlTarget,
                    responseBody : JSON.stringify(resp, null,  2),
                    requestBody  : JSON.stringify(_apiObject.requestParams, null,  2)
                }
               
                if(_apiObject.headers){
                    data.headers = JSON.stringify(_apiObject.headers, null, 2)
                }

                if(_func)
                    _func(data, this)

                resolve(data)
            }, 1000)
        });
    }

    // result(){
    //     let lastResult = this.resultArray;
    //     return lastResult;
    // }

    
}

export function giveConsoleComment(_message: String, _optionalObject?: any){
    if(appDebug){
        console.log(_message, _optionalObject?_optionalObject:(_optionalObject==undefined?"undefined":""))
    }
}

export interface API_Object{
    method          : 'GET' | 'POST' | 'PUT' | 'DELETE';
    requestParams   : any;
    urlTarget       : String;
    expectedResults : any;
    headers        ?: any;
}

export interface RESP_Data{
    status       : String,
    urlTarget    : String,
    responseBody : any,
    requestBody  : any,
    headers     ?: any,
}

