import { API_Object } from '../../services/api-test-scenario';
import { api_url } from '../../services/environtment.api';

export const _1guestLoginTest: API_Object[]= [
    { 
      method          : "GET",
      urlTarget       : api_url() + "member/guest_login",
      requestParams   : {},
      expectedResults : {}
    },
    { 
      method          : "GET",
      urlTarget       : api_url() + "masterload",
      requestParams   : {},
      expectedResults : {}
    }
  ]