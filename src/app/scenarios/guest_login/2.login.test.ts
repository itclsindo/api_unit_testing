import { API_Object } from '../../services/api-test-scenario';
import { api_url } from '../../services/environtment.api';

export const _2loginTest: API_Object[]= [
    { 
      method          : "POST",
      urlTarget       : api_url() + "member/login",
      requestParams   : {
        email: "2008madik@gmail.com",
        password: "123456"
      },
      expectedResults : {}
    },
    { 
      method          : "GET",
      urlTarget       : api_url() + "masterload",
      requestParams   : {},
      expectedResults : {}
    }
  ]