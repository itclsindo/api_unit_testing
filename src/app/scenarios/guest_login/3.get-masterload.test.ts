import { API_Object } from '../../services/api-test-scenario';
import { api_url } from '../../services/environtment.api';

export const _3getMasterloadTest: [API_Object]= [
    { 
      method          : "GET",
      urlTarget       : api_url() + "masterload",
      requestParams   : {},
      expectedResults : {}
    }
  ]