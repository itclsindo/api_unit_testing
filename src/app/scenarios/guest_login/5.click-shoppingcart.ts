import { API_Object } from '../../services/api-test-scenario';
import { api_url } from '../../services/environtment.api';

export const _5clickshoppingcart: API_Object[]= [
    { 
      method          : "GET",
      urlTarget       : api_url() + "/f3/v1/shoppingcart",
      requestParams   : {},
      expectedResults : {}
    },

  ]