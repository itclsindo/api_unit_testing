import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginTestComponentComponent } from './pages/login-test-component/login-test-component.component';
import { HomeComponent } from './pages/home/home.component';

const routes: Routes = [
  {path: 'test1', component: LoginTestComponentComponent},
  {path: ''     , component: HomeComponent},
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
