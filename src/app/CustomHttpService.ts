import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable()
export class CustomHttpService {

  constructor(private httpClient: HttpClient) { }

  getSingle<T>(id: number) {
    

    return this.httpClient.post<T>('https://api2.locard.co.id/f3/v1/member/guest_login', id);
  }

  post<T>(item: any) {
    console.log("data.number", item)
    let c = this.httpClient.post<T>('https://api2.locard.co.id/f3/v1/member/guest_login', item);
    console.log("data.c", c)
    return c;
  }

  put<T>(id: number, item: any) {
    return this.httpClient.put<T>(`http://replace.with.api/anything/${id}`, item);
  }

  delete(id: number) {
    return this.httpClient.delete(`http://replace.with.api/anything/${id}`);
  }
}