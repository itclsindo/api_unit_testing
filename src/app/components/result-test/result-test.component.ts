import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-result-test',
  templateUrl: './result-test.component.html',
  styleUrls: ['./result-test.component.scss']
})
export class ResultTestComponent implements OnInit {

  @Input() testBody
  @Input() scenarios
  @Input() describes
 
  constructor() {
   
   }

  ngOnInit() {
   
    console.log("this.body", this.testBody);
  }

}
