import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  host: {'[class]': '"hostSelector"'},
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  listOfLinks = [
    {target: '/test1', label:'test 1'}
  ]
  
  constructor(private router:Router) {
   
   }

  ngOnInit() {
  }

  goTo(url){
    this.router.navigateByUrl(url);
  }

}
