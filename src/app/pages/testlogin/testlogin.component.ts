import { Component, OnInit } from '@angular/core';
import { _1guestlogin } from 'src/app/scenarios/scenario2/guest-login';
import { ApiTestScenario } from 'src/app/services/api-test-scenario';
import { async } from '@angular/core/testing';
import { ResultTestComponent } from 'src/app/components/result-test/result-test.component';

@Component({
  selector: 'app-testlogin',
  templateUrl: './testlogin.component.html',
  styleUrls: ['./testlogin.component.scss']
})
export class TestloginComponent implements OnInit {

  testBody = [];
  scenarios = [];
  describes = [];

  constructor(private apiTestScenario: ApiTestScenario) { }

  ngOnInit() {
    this.firstLoad();
  }
  async firstLoad(){
     let apiscenario = _1guestlogin
     this.scenarios.push (this.apiTestScenario.setScenario("Saya sedang testing Untuk Member Guest Login", this.testBody, true));
    let token = ""
     await this.scenarios[0].setDescribe("test Login guest atau non member", async (_this)=> {
      this.describes.push(_this.getDescribe());
      this.testBody.push([]);
      this.testBody[0].push (await _this.runAPICall(apiscenario[0], async(_data)=>{
      token = (JSON.parse(_data.responseBody).result.token)
      }))
     })

     apiscenario[1].headers = {
       Authorization: token
     }
     await this.scenarios[0].setDescribe("Test Masterload", async (_this)=> {
      this.describes.push(_this.getDescribe());
      this.testBody.push([]);
      this.testBody[0].push (await _this.runAPICall(apiscenario[1], async(_data)=>{
        console.log(JSON.parse(_data.responseBody).result.feeds[0].product[0].fixed_price)
      }))
     })

  }
}
