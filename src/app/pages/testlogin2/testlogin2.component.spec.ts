import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Testlogin2Component } from './testlogin2.component';

describe('Testlogin2Component', () => {
  let component: Testlogin2Component;
  let fixture: ComponentFixture<Testlogin2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Testlogin2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Testlogin2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
