import { Component, OnInit } from '@angular/core';
import { ApiTestScenario } from 'src/app/services/api-test-scenario';
import { _1tamu } from 'src/app/scenarios/scenario3/tamu';
import { async } from '@angular/core/testing';

@Component({
  selector: 'app-testlogin2',
  templateUrl: './testlogin2.component.html',
  styleUrls: ['./testlogin2.component.scss']
})
export class Testlogin2Component implements OnInit {

  testBody  = [];
  scenarios = [];
  describes = [];

  constructor(private apiTestScenario: ApiTestScenario) { }

  ngOnInit() {
    this.firstLoad();
  }
  async firstLoad(){
    let apiscenario = _1tamu
    this.scenarios.push (this.apiTestScenario.setScenario("LAGI TES BOSQ",this.testBody,true));

    await this.scenarios[0].setDescribe("test Login guest atau non member",async (_this)=>{
      this.describes.push(_this.getDescribe());
    })
  }

}
