import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginTestComponentComponent } from './login-test-component.component';

describe('LoginTestComponentComponent', () => {
  let component: LoginTestComponentComponent;
  let fixture: ComponentFixture<LoginTestComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoginTestComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginTestComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
