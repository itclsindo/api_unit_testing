import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MasterService } from '../../services/master.service';
import { ApiTestScenario } from '../../services/api-test-scenario';
import { _1guestLoginTest } from '../../scenarios/guest_login/1.guest-login.test';
import { _2loginTest } from '../../scenarios/guest_login/2.login.test';
import { _3getMasterloadTest } from 'src/app/scenarios/guest_login/3.get-masterload.test';
import { _4clicklogin } from 'src/app/scenarios/guest_login/4.click-login';
import { _5clickshoppingcart } from 'src/app/scenarios/guest_login/5.click-shoppingcart';




@Component({
  selector: 'app-login-test-component',
  templateUrl: './login-test-component.component.html',
  styleUrls: ['./login-test-component.component.scss'],
  host: {'[class]': '"col-md-12"'}
})
export class LoginTestComponentComponent implements OnInit {

  title = 'ApiUnitTesting';
  testBody = [];
  scenarios = [];
  describes = [];


  constructor(private httpClient: HttpClient, 
              private masterService: MasterService,
              private apiTestScenario: ApiTestScenario
              ) {}

  ngOnInit(){
    this.firstLoad();
  }

  async firstLoad(){
      const _apiObject = _5clickshoppingcart

      let newData;

      await this.scenarios[0].setDescribe("Isi Shopping cart", async (_this) => {
        this.describes.push(_this.getDescribe());
        this.testBody.push([]);
        this.testBody[0].push (await _this.runAPICall( _apiObject[0], (data)=>{
          newData = JSON.parse(data.responseBody).result;
        
        }))
        _apiObject[1].headers = {
            'authorization': newData.Token
        }

        await this.scenarios[0].setDescribe("Sekarang kita get Masterload dari Token  :<br/> ", async (_this) => {
            this.describes.push(_this.getDescribe());
            this.testBody.push([]);
            this.testBody[1].push (await _this.runAPICall( _apiObject[1], (data)=>{
              // newData = JSON.parse(data.responseBody).result;
      
            }))
          })
        
      })

  }
}
